package com.gautamverma.builder;

import com.gautamverma.node.LeafNode;

public class Number extends Symbol {

	int value;
	
	public Number(int value)
	{
		super(null, null);
		this.value = value;
	}

	@Override
	public LeafNode build() {
		return new LeafNode(value);
	}
}
