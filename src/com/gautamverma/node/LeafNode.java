package com.gautamverma.node;

/**
 * It hold the value of a variable
 * @author gautam
 *
 */
public class LeafNode extends ComponentNode {

	int value;
	
	public LeafNode(int value) {
		super();
		this.value = value;
	}
	
	@Override
	public void accept() {
		
	}

}
