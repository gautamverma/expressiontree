package com.gautamverma.node;

/**
 * The base class of node in the Expression Tree
 * @author gautam
 *
 */
public abstract class ComponentNode {

	/**
	 * It accepts a visitor and call the visitor visit method
	 * {Thus we can various methods like print, evaluate without adding these on node}
	 */
	public abstract void accept();
}

