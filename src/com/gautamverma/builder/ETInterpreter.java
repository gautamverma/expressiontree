package com.gautamverma.builder;

import com.gautamverma.ExpressionTree;

/**
 * This is the main Interpreter class which will read the expressions 
 * and build a parse tree from it.
 * 
 * @author Gautam Verma
 *
 */
public abstract class ETInterpreter {

	/**
	 * This builds a parse tree using expression and then build the Expression Tree 
	 * using the parse tree.
	 * 		Dependency on this method: Create a parse tree using the base class 
	 * 									symbol
	 * @param context
	 * @param expression
	 * @return
	 */
	public abstract ExpressionTree interpret(ETIntrepreterContext context, final String expression);
	
}
