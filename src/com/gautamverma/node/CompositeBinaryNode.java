package com.gautamverma.node;

public abstract class CompositeBinaryNode extends CompositeUnaryNode {

	// This child can be any node
	ComponentNode leftchild;
	
	public CompositeBinaryNode(ComponentNode leftchild, ComponentNode rightchild) 
	{
		super(rightchild);
		this.leftchild = leftchild;
	}
}
