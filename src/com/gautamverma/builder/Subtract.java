package com.gautamverma.builder;

import com.gautamverma.node.CompositeSubtractNode;

public class Subtract extends Operator {

	public Subtract(Symbol leftchild, Symbol rightchild) {
		super(leftchild, rightchild);
	}

	@Override
	public CompositeSubtractNode build() {
		return new CompositeSubtractNode( leftchild.build(), rightchild.build());
	}

}
