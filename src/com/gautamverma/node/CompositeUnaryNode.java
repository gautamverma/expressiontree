package com.gautamverma.node;

public abstract class CompositeUnaryNode extends ComponentNode {

	ComponentNode rightchild;
	public CompositeUnaryNode(ComponentNode rightchild) {
		this.rightchild = rightchild;
	}
}
