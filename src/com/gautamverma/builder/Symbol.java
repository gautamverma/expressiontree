package com.gautamverma.builder;

import com.gautamverma.node.ComponentNode;

public abstract class Symbol {

	Symbol leftchild;
	Symbol rightchild;
	
	public Symbol(Symbol leftchild, Symbol rightchild)
	{
		this.leftchild = leftchild;
		this.rightchild = rightchild;
	}
	
	/**
	 * It creates the appropriate tree node from the parse tree node
	 * @return
	 */
	public abstract ComponentNode build();
}
