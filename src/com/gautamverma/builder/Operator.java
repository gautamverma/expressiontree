package com.gautamverma.builder;

public abstract class Operator extends Symbol {

	public Operator(Symbol leftchild, Symbol rightchild) {
		super(leftchild, rightchild);
	}

}
